class Company {
  final String imageUrl;
  final String url;
  final String percent;
  final String sum;

  Company({
    required this.url,
    required this.imageUrl,
    required this.sum,
    required this.percent,
  });

  factory Company.fromJson(Map<String, dynamic> map) => Company(
        url: map['url'],
        imageUrl: map['image_url'],
        sum: map['sum_one'],
        percent: map['percent'],
      );
}
