import 'dart:convert';

import 'package:dengatogo/colors.dart';
import 'package:dengatogo/company.dart';
import 'package:dengatogo/ui/company_item.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class MainScreen extends StatefulWidget {
  const MainScreen({super.key});

  @override
  State<MainScreen> createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  Future<List<Company>> _getCompany() async {
    var response = await http.get(
      Uri.parse('http://ouitruzade.ru/pp/api.json'),
    );
    if (response.statusCode == 200) {
      List json = jsonDecode(utf8.decode(response.bodyBytes));
      List<Company> result = json.map((e) => Company.fromJson(e)).toList();
      return result;
    }
    return [];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Займы онлайн'),
        backgroundColor: AppColors.main,
        centerTitle: true,
      ),
      body: FutureBuilder<List<Company>>(
        future: _getCompany(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            var companies = snapshot.data ?? [];
            return ListView.builder(
              padding: const EdgeInsets.symmetric(vertical: 12),
              itemCount: companies.length,
              itemBuilder: (context, i) => CompanyItem(companies[i]),
            );
          }
          return const Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
    );
  }
}
