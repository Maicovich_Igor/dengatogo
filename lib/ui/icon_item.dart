import 'package:flutter/material.dart';

class IconItem extends StatelessWidget {
  final String icon;
  final String text;
  const IconItem({
    super.key,
    required this.icon,
    required this.text,
  });

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image.asset(icon),
          const SizedBox(width: 6),
          Text(
            text,
            style: const TextStyle(
              fontSize: 11,
              color: Color(0xFFB0B0B0),
              fontWeight: FontWeight.w600,
            ),
          )
        ],
      ),
    );
  }
}
