import 'package:cached_network_image/cached_network_image.dart';
import 'package:dengatogo/colors.dart';
import 'package:dengatogo/company.dart';
import 'package:dengatogo/ui/icon_item.dart';
import 'package:dengatogo/ui/info_item.dart';
import 'package:dengatogo/webview_screen.dart';
import 'package:flutter/material.dart';

class CompanyItem extends StatelessWidget {
  final Company company;
  const CompanyItem(
    this.company, {
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: const EdgeInsets.symmetric(horizontal: 24, vertical: 12),
      elevation: 20,
      color: Colors.white,
      shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
        topRight: Radius.circular(20),
        bottomLeft: Radius.circular(20),
      )),
      child: Padding(
        padding: const EdgeInsets.all(24),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            CachedNetworkImage(
              imageUrl: company.imageUrl,
              height: 50,
            ),
            const SizedBox(height: 16),
            InfoItem(title: 'Сумма', value: company.sum),
            const SizedBox(height: 12),
            InfoItem(title: 'Ставка', value: company.percent),
            const SizedBox(height: 16),
            ElevatedButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => WebViewScreen(
                      url: company.url,
                    ),
                  ),
                );
              },
              style: ElevatedButton.styleFrom(
                  backgroundColor: AppColors.main,
                  padding: const EdgeInsets.all(16),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10))),
              child: const Text(
                'Получить деньги',
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.w700),
              ),
            ),
            const SizedBox(height: 16),
            const Row(
              children: [
                IconItem(icon: 'assets/info.png', text: 'Пример\nрасчета'),
                IconItem(icon: 'assets/doc.png', text: 'Паспорт\nкредита'),
                IconItem(icon: 'assets/timer.png', text: 'График\nплатежей'),
              ],
            )
          ],
        ),
      ),
    );
  }
}
